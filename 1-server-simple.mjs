import log from '@ajar/marker'; 
import http from 'http';
import url from 'url';
// const PORT = process.env.PORT;
// const HOST = process.env.HOST;
const { PORT, HOST } = process.env;
//console.log(process.env);

//create an http web server
const server = http.createServer( (req,res)=> {
    const fullUrl = 'http://' + HOST + ":"+ PORT + req.url;
    const myURL = new URL(fullUrl);
    const queryData = url.parse(req.url, true).query;
    res.setHeader("agenda","political");
    res.setHeader("anything","goes");
    res.setHeader("some-single-header", "some-single-value");
    res.setHeader('Content-Type','application/json')
    const responseObj = buildResponseObj(myURL,req,queryData);
    res.statusCode = 200;  
    res.end(JSON.stringify(responseObj));

});

const buildResponseObj = (givenUrl,request,queryData) =>{
    return {
        href : givenUrl.href,
        url : request.url,
        method : request.method,
        host : givenUrl.host,
        protocol : givenUrl.protocol,
        httpVersion : request.httpVersion,
        pathname :givenUrl.pathname,
        querystring :{
            month : queryData.month,
            temp : queryData.temp
        },
        "user-agent" : request.headers['user-agent'],
        connections : request.headers.connection
    }
}

//have the server listen to a given port
server.listen(PORT,HOST, err => {
    if(err) log.error(err);
    else log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});
